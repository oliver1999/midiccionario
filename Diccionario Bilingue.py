
if __name__ == "__main__":
    print("Mi diccionario")
    midiccio = {
        "gato" : ["cat","Mamífero felino de tamaño generalmente pequeño, cuerpo flexible.","Feline mammal of generally small size, flexible body.\n"],
        "perro" : ["dog","","Mamífero carnívoro doméstico de la familia de los cánidos que se caracteriza por tener los sentidos del olfato","Domestic carnivorous mammal of the family of canids that is characterized by having the senses of smell"],
        "carro" : ["car","Vehículo formado por una plataforma con barandillas montada sobre ruedas\n","Vehicle formed by a platform with railings mounted on wheels"],
    }
    print(midiccio)

    print(midiccio["gato"])

    del midiccio["perro"]
    print(midiccio)

    midiccio.clear()
    print(midiccio)

    midiccio2 = {
        "lobo": ["wolf", "especie de mamífero placentario del orden de los carnívoros. \n","species of placental mammal of the order of carnivores.\n"],
        "leon": ["lion", "mamífero carnívoro de la familia de los félidos.\n","carnivorous mammal of the felidae family"],
    }
    del midiccio
    midiccio = list(midiccio2)
    print(midiccio)

    midiccio3 = {
        "luz" : ["light", "parte de la radiación electromagnética que puede ser percibida por el ojo humano.\n", "part of the electromagnetic radiation that can be perceived by the human eye.\n"]
    }
    print(midiccio2)
    print(midiccio)
    print(midiccio3)
